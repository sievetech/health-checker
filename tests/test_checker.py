from unittest import TestCase
from healthchecker.checker import check, CHECK_TOKEN, CheckCase


class CheckTest(TestCase):
    def test_add_token_to_function(self):
        func = lambda: 1
        decorated_func = check(func)

        self.assertEqual(True, getattr(decorated_func, CHECK_TOKEN))

    def test_function_continues_working_as_expected(self):
        func = lambda: 10
        decorated_func = check(func)

        self.assertEqual(func(), decorated_func())


class CheckCaseTest(TestCase):
    def test_report_gather_results_from_all_decorated_methods(self):
        class Spam(CheckCase):
            def ignore_me(self):
                return True

            @check
            def look_at_me(self):
                return True

            @check
            def dont_forget_me(self):
                return False

            @check
            def i_am_not_special(self):
                return 'Wow'

        spam = Spam()
        spam.check()

        self.assertEqual({'look_at_me': True, 'dont_forget_me': False,
                          'i_am_not_special': 'Wow'},
                         spam.check_report)

    def test_check_returns_true_if_all_results_are_true_like(self):
        class Spam(CheckCase):
            egg = check(lambda x: True)
            fish = check(lambda x: 'Yay')

        spam = Spam()

        self.assertTrue(spam.check())

    def test_check_returns_false_is_any_check_method_raises_exception(self):
        class Spam(CheckCase):
            @check
            def egg(self):
                raise Exception()

            @check
            def fish(self):
                return 'Yay'

        spam = Spam()

        self.assertFalse(spam.check())
        self.assertEqual({'egg': False, 'fish': 'Yay'}, spam.check_report)

    def test_check_populates_report(self):
        class Spam(CheckCase):
            egg = check(lambda x: True)

        spam = Spam()

        self.assertEqual(0, len(spam.check_report))
        spam.check()
        self.assertEqual(1, len(spam.check_report))

    def test_has_succeeded_return_true_if_everything_is_true_like(self):
        class Spam(CheckCase):
            egg = check(lambda x: True)
            fish = check(lambda x: 'Yay')

        class Ham(CheckCase):
            egg = check(lambda x: False)

        spam = Spam()
        ham = Ham()

        spam.check()
        ham.check()

        self.assertTrue(spam.has_succeeded())
        self.assertFalse(ham.has_succeeded())

    def test_has_succeeded_returns_true_if_there_is_nothing_in_report(self):
        class Spam(CheckCase):
            pass

        spam = Spam()

        self.assertTrue(spam.has_succeeded())
        self.assertEqual({}, spam.check_report)
