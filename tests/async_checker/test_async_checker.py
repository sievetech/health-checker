import asyncio
import unittest
from healthchecker import check
from healthchecker.async_checker import AsyncCheckCase


class CheckCaseTest(unittest.TestCase):
    def setUp(self):
        self.loop = asyncio.get_event_loop()

    def test_report_gather_results_from_all_decorated_methods(self):
        class Spam(AsyncCheckCase):
            loop = self.loop

            def ignore_me(self):
                raise Exception("I'll never be called")

            @check
            async def look_at_me(self):
                return True

            @check
            async def dont_forget_me(self):
                await asyncio.sleep(0.05)
                return False

            @check
            async def i_am_not_special(self):
                await asyncio.sleep(0.03)
                return 'Wow'

        spam = Spam()
        self.loop.run_until_complete(spam.check())

        expected_report = {
            'look_at_me': True,
            'dont_forget_me': False,
            'i_am_not_special': 'Wow'
        }
        self.assertEqual(expected_report, spam.check_report)

    def test_check_returns_true_if_all_results_are_true_like(self):
        class Spam(AsyncCheckCase):
            loop = self.loop

            @check
            async def maldito(self):
                return True

            @check
            async def matos(self):
                return "Yay"

            @check
            async def me_ferrou(self):
                return 1234

        spam = Spam()
        self.loop.run_until_complete(spam.check())
        self.assertTrue(spam.has_succeeded())

    def test_check_returns_false_is_any_check_method_raises_exception(self):
        class Spam(AsyncCheckCase):
            loop = self.loop

            @check
            async def egg(self):
                raise Exception()

            @check
            async def fish(self):
                return 'Yay'

        spam = Spam()
        self.loop.run_until_complete(spam.check())

        self.assertFalse(spam.has_succeeded())
        self.assertEqual({'egg': False, 'fish': 'Yay'}, spam.check_report)

    def test_check_populates_report(self):
        class Spam(AsyncCheckCase):
            loop = self.loop

            @check
            async def egg(self):
                return True

        spam = Spam()

        self.assertEqual(0, len(spam.check_report))
        self.loop.run_until_complete(spam.check())
        self.assertEqual(1, len(spam.check_report))

    def test_has_succeeded_return_true_if_everything_is_true_like(self):
        class Spam(AsyncCheckCase):
            loop = self.loop

            @check
            async def egg(self):
                return True

            @check
            async def fish(self):
                return 'Yay'

        class Ham(AsyncCheckCase):
            loop = self.loop

            @check
            async def egg(self):
                return False

        spam = Spam()
        ham = Ham()

        tasks = asyncio.gather(spam.check(), ham.check())
        self.loop.run_until_complete(tasks)

        self.assertTrue(spam.has_succeeded())
        self.assertFalse(ham.has_succeeded())

    def test_has_succeeded_returns_true_if_there_is_nothing_in_report(self):
        class Spam(AsyncCheckCase):
            loop = self.loop

        spam = Spam()

        self.assertTrue(spam.has_succeeded())
        self.assertEqual({}, spam.check_report)
